import React from "react";
import NewEntry from "./components/entry";
import InputTForm from "./components/inputForm";
import {WrappCalendar} from './styles/style';
import Calendar from 'rc-calendar';
import {
  theme,
  Title,
  Slogan,
  Container,
  Wrapper,
  Footer,
  ItemFooter,
  BtnFooter,
  HeadWrapper,
  MyCalendar,
  MobileMenu
} from './styles/style';
import { ThemeProvider } from "styled-components";

class App extends React.Component {
  constructor(props){
    super(props);
    console.log(props);
    this.state ={
      count: 0,
      job : "",
    }
  }
  getJob(job){
    this.setState({
      job:job
    });
  }
  addMenu(){
    let hic = document.getElementById("mMenu");
    hic.parentElement.classList.add("ahii");
    hic.classList.remove("fa-bars");
    hic.classList.add("fa-times");
  }
  render() {
    return (
      <div className="App">
        <ThemeProvider theme={theme}>
          <Container>
            <section>
            <Wrapper>
              <HeadWrapper>
              <MobileMenu >
              <i className="fa fa-bars" aria-hidden="true" id="mMenu" onClick={this.addMenu} style={{position: 'absolute',top: 32 ,left: 20}}></i>
              </MobileMenu>
              <Title>Activities</Title>
              <MyCalendar>
                <i className="fa fa-calendar" aria-hidden="true" ></i>
                <WrappCalendar>
                  <Calendar/>
                </WrappCalendar>
                {/* <Gcalendar/> */}
                {/* {this.state.onClicked && <Gcalendar/>} */}
              </MyCalendar>
              </HeadWrapper>
              <Slogan>Take a look everything around you before left</Slogan>
            </Wrapper>
            </section>
            <section>
              <InputTForm getJob1={this.getJob.bind(this)}/>
            </section>
            <section>
              <NewEntry/>
            </section>
            <section>
              <Footer>
                <BtnFooter>All</BtnFooter>
                <ItemFooter>items: {this.state.count}</ItemFooter>
                <BtnFooter>delete</BtnFooter>
              </Footer>
            </section>
          </Container>
        </ThemeProvider>
      </div>
    );
  }
}

export default App;