import styled from "styled-components";

export const theme = {
  colorMain: "#394264",
  colorText: "#fff",
  colorInput: "#a0a2ab",
  colorDeleteBtn: "#e64c65",
  colorAdd: "#11a8ab",
  colorEditBtn: "#50597b",
  colorSlogun: "#6b7494",
  fontSizeMain: "12px",
  fontSize: "13px",
  fontWeight: "700",
  borderRadius: "3px",
};

export const Title = styled.h1 `
font-size: 1.5em;
text-align: center;
color: ${props => props.theme.colorText };
margin: 0 auto;
`;
export const Container = styled.div `
  max-width: 1140px;
  width: 100%;
  margin: 0 auto;
`;

export const Slogan = styled.p `
  color: ${props => props.theme.colorSlogun} ;
  margin-top: 5px;
  font-size: ${props => props.theme.fontSizeMain};
  text-align: center;
`;
export const Input = styled.input `
  padding: 0.8em 40px 0.8em 10px;
  border-radius: ${props => props.theme.borderRadius};
  background: ${props => props.theme.colorEditBtn};
  border: none;
  color: ${props => props.theme.colorText};
  width: 100%;
  height: 100%;
  margin: auto 0;
  outline: none;
  &::placeholder {
    color: ${props => props.theme.colorInput} ;
  }
`;

const Button = styled.button `
  cursor: pointer;
  background: ${props =>props.theme.colorAdd } ;
  font-size: ${props =>props.theme.fontSizeMain };
  border: none;
  border-radius: ${props => props.theme.borderRadius};
  color: ${props => props.theme.colorText};
  text-transform: uppercase;
  font-weight: ${props => props.theme.fontWeight} ;
  transition: 0.5s all ease;
  white-space: nowrap;
  outline: none;
`;
// const revertTheme = ({colorAdd, colorText}) =>({
//   colorAdd: colorText,
//   colorText: colorAdd
// })
export const BtnAdd = styled(Button)`
  margin: 1em 0 1em 1em;
  padding: .8em;
  border: 2px solid transparent;
  &:hover{
    background: ${props =>props.theme.colorText};
    color: ${props =>props.theme.colorAdd};
  }
`;
export const Wrapper = styled.div `
  max-height: 100px;
  height: 100%;
  padding: 30px 0;
`;
export const HeadWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;
export const MobileMenu = styled.div`
  color: ${props =>props.theme.colorText};
  /* margin: 6px 0; */
  font-size: 24px;
  transition: .5s all ease;
  margin: 0;
  position: absolute;
  background: ${props =>props.theme.colorEditBtn};
  height: 100vh;
  top: 0;
  left: 0;
  width: 0;
  box-shadow: 0 1px 2px 0 rgba(0,0,0,0.5);
  &.ahii{
    width: 85%;
    z-index: 2;
  }
  `
export const MyCalendar = styled.div`
  color: ${props => props.theme.colorText};
  margin: auto 0;
  font-size: 24px;
  cursor: pointer;
`
export const WrapperInput = styled.div `
  display: flex;
  justify-content: space-between;
`;
export const Footer = styled.div `
  margin-top: 20px;
  background: ${props =>props.theme.colorEditBtn};
  /* border-radius: ${props =>props.theme.borderRadius}; */
  display: flex;
  justify-content: space-between;
  color: ${props =>props.theme.colorText};
  text-transform: uppercase;
  font-size: ${props =>props.theme.fontSizeMain};
  font-weight: ${props =>props.theme.fontWeight};
  font-style: italic;
  position: fixed;
    width: 100%;
    left: 0;
    top: auto;
    bottom: 0;
    z-index: 1;
    box-shadow: 0 1px 20px 20px rgba(0,0,0,0.5);
`;
export const BtnFooter = styled(Button)`
  background: none;
  padding: 10px;
  font-style: italic;
  
`
export const ItemFooter = styled.div `
  margin: auto 0 auto 2px;
  /* position: absolute;
    width: 100%;
    top: auto;
    bottom: 0;
    left: 0; */
`;

export const WrapperItem = styled.div `
font-size: ${props =>props.theme.fontSizeMain};
color: ${props =>props.theme.colorText} ;
background: ${props =>props.theme.colorMain} ;
display: flex;
justify-content: space-between;
margin-top: 2px;
border-radius: ${props =>props.theme.borderRadius};
`
export const WrapInput = styled.div `
margin: auto 10px;
`
export const NameItem = styled.p `
padding: 3px 10px;
overflow: hidden;
white-space: nowrap;
max-width: 120px;
`;
export const Wrapp1 = styled.div `
    display: flex;
    justify-content: space-between;
`;
export const WrappCalendar = styled.div `
    position: absolute;
    bottom: 0;
    right: 0;
    
`;
export const Wrapp2 = styled.div `
    display: flex;
    justify-content: space-between;
`;
export const Button1 = styled(Button)`
    background: ${props =>props.theme.colorEditBtn};
    border: none;
    border-radius: unset;
    padding: 0 15px;
    font-size: 11px;
    
`;
export const Button2 = styled(Button)`
    border: none;
    background: ${props =>props.theme.colorDeleteBtn};
    padding: 0 15px;
    border-top-left-radius: unset;
    border-bottom-left-radius: unset;
    border-bottom-right-radius: ${props =>props.theme.borderRadius};
    border-top-right-radius: ${props =>props.theme.borderRadius};
    font-size: 11px;
`;