import React from 'react';
import {
    theme,
    WrapperInput,
    Input,
    BtnAdd
  } from '../styles/style';
  import { ThemeProvider } from "styled-components";


class InputTForm extends React.Component{
    constructor(props) {
        super(props);
        console.log(props);
      }
      componentDidMount(){//fire
        document.getElementById('save').addEventListener('click', ()=>{
            let job = document.getElementById('job').value;
            if(this.props.getJob1 && job!== ''){
              this.props.getJob1(job);
              document.getElementById('job').value = '';
            }
        });
      }
    render(){
        return (
            <ThemeProvider theme={theme}>
                <WrapperInput >
                    <Input placeholder="input your action" id="job" />
                    <BtnAdd id="save" value ="save" >create new</BtnAdd>
              </WrapperInput>
            </ThemeProvider>
        )
    }  
}
export  default InputTForm;