import React from 'react'
import {
    WrapperItem,
    Wrapp1,
    WrapInput,
    NameItem,
    Wrapp2,
    Button1,
    Button2,
    theme
} from '../styles/style';
import { ThemeProvider } from 'styled-components';

class newEntry extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            list: [],
            data: "",
        };
    };
componentWillReceiveProps(props){
    let {list} = this.state;
    list.unshift(props.data);
    this.setState(
        {
            list
        }
    )
}
    render(){
        return(
            <div>
                {this.state.list.map(() =>{
                    return (
                    <Items  key={Math.random()}/>
                    )
                })}
            </div>
        )
    }
}
export default newEntry;

export  class Items extends React.Component{
    render(){
        return (
                <ThemeProvider theme={theme}>
                    <WrapperItem>
                        <Wrapp1>
                            <WrapInput>
                                <input type="checkbox" id="input" name="inputItem" />
                            </WrapInput>
                            <NameItem>{this.props.data}</NameItem>
                        </Wrapp1>
                        <Wrapp2>
                            <Button1 id="edit" >Edit</Button1>
                            <Button2 value="Delete" id="delete">delete</Button2>
                        </Wrapp2>
                    </WrapperItem>
                </ThemeProvider>
        )
    }  
}